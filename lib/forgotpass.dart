import 'package:flutter/material.dart';
import 'sendotp.dart';

class ForgotPassPage extends StatefulWidget {
  const ForgotPassPage({Key? key}) : super(key: key);

  @override
  State<ForgotPassPage> createState() => _ForgotPassPageState();
}

class _ForgotPassPageState extends State<ForgotPassPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children:  [
          Positioned(
            top: 0,
            right: 0,
            left: 0,
            child: Container(
              height: MediaQuery.of(context).size.height- 300,
              decoration: BoxDecoration(
                color: Color.fromRGBO(238, 129, 116, 1),
                borderRadius: BorderRadius.only(bottomRight: Radius.elliptical(30,0),bottomLeft: Radius.circular(200),),
              ),
            )
          ),
          Positioned(
            top: 180,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              height: 400,
              width: MediaQuery.of(context).size.width-40,
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(50),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    blurRadius: 15,
                    spreadRadius: 5,
                  ),
                ],
              ),
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 50,),
                  Text(
                    'Forgot',
                    style: TextStyle(
                      fontSize: 21,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'roboto',
                      color: Colors.black,
                      
                    ),
                  ),
                  Text(
                    "Password?",
                    style: TextStyle(
                      fontSize: 21,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'roboto',
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(height: 25,),
                  Text(
                    "Don’t worry! It happens. Please enter the phone",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      fontFamily: 'roboto',
                      color: Color.fromRGBO(91, 88, 88, 1),
                    ),
                  ),
                  Text(
                    "number we will send the OTP in this phone number.",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      fontFamily: 'roboto',
                      color: Color.fromRGBO(91, 88, 88, 1),
                    ),
                  ),
                  SizedBox(height: 20,),
                  Container(
                    child: TextField(
                      style: 
                        TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 10,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(126, 126, 126, 1)
                        ),
                      decoration: InputDecoration(
                        hintText: 'Enter the phone number',
                      ),
                      ),
                    ),
                  SizedBox(height: 50,),
                  Container(
                    alignment: Alignment.center,
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => otppage()),
                        );
                      },
                      child: Text(
                        'Continue',
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'roboto',
                          color: Colors.white,
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        primary: Color.fromRGBO(238, 129, 116, 1),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                        minimumSize: Size(180, 50),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}