import 'package:flutter/material.dart';
import 'forgotpass.dart';

class otppage extends StatefulWidget {
  const otppage({super.key});

  @override
  State<otppage> createState() => _otppageState();
}

class _otppageState extends State<otppage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children:  [
          Positioned(
            top: 0,
            right: 0,
            left: 0,
            child: Container(
              height: MediaQuery.of(context).size.height- 350,
              decoration: BoxDecoration(
                color: Color.fromRGBO(238, 129, 116, 1),
                borderRadius: BorderRadius.only(bottomRight: Radius.elliptical(30,0),bottomLeft: Radius.circular(200),),
              ),
            )
          ),
          Positioned(
            top: 150,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              height: 400,
              width: MediaQuery.of(context).size.width-40,
              margin: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(50),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    blurRadius: 15,
                    spreadRadius: 5,
                  ),
                ],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                // crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 50,),
                  Text(
                    'OTP VERIFICATION',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                      fontFamily: 'roboto',
                      color: Colors.black,
                      
                    ),
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Enter the OTP sent to',
                        style: TextStyle(
                          fontFamily: 'roboto',
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(78, 77, 77, 1),
                        ),
                      ),
                      Text(
                        ' - +62 85337272090',
                        style: TextStyle(
                          fontFamily: 'roboto', 
                          fontSize: 12,
                          fontWeight: FontWeight.w700,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 30,),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        _textFieldOTP(first: true , last: false),
                        SizedBox(width: 9,),
                        _textFieldOTP(first: false, last: false),
                        SizedBox(width: 9,),
                        _textFieldOTP(first: false, last: false),
                        SizedBox(width: 9,),
                        _textFieldOTP(first: false, last: true),
                      ],
                  ),
                  SizedBox(height: 15,),
                  Text(
                    '01:20',
                    style: TextStyle(
                      fontFamily: 'roboto',
                      fontSize: 11,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(78, 77, 77, 1),
                    ),
                  ),
                  SizedBox(height: 25,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Don’t receive code?',
                        style: TextStyle(
                          fontFamily: 'roboto',
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(78, 77, 77, 1),
                        ),
                      ),
                      Text(
                        '  Re-send',
                        style: TextStyle(
                          fontFamily: 'roboto', 
                          fontSize: 13,
                          fontWeight: FontWeight.w600,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 35,),
                  Container(
                    alignment: Alignment.center,
                    child: ElevatedButton(
                      onPressed: () {
                        showAlertDialog(context);
                      },
                      child: Text(
                        'Submit',
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'roboto',
                          color: Colors.white,
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        primary: Color.fromRGBO(238, 129, 116, 1),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                        minimumSize: Size(180, 50),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
  Widget _textFieldOTP({required bool first, last}) {
    return Container(
      height: 60,
      child: AspectRatio(
        aspectRatio: 0.9,
        child: TextField(
          autofocus: true,
          onChanged: (value) {
            if (value.length == 1 && last == false) {
              FocusScope.of(context).nextFocus();
            }
            if (value.length == 0 && first == false) {
              FocusScope.of(context).previousFocus();
            }
          },
          showCursor: false,
          readOnly: false,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18, 
            fontWeight: FontWeight.w400,
            fontFamily: 'roboto',
          ),
          keyboardType: TextInputType.number,
          maxLength: 1,
          decoration: InputDecoration(
            counter: Offstage(),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(width: 2, color: Colors.black12),
                borderRadius: BorderRadius.circular(12)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(width: 2, color: Colors.black26),
                borderRadius: BorderRadius.circular(12)),
          ),
        ),
      ),
    );
  }
  void showAlertDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Container(
            width: 150,
            height: 310,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(50)),
            ),
            //image
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  image: AssetImage('assets/image/verified.png'),
                  width: 100,
                  height: 100,
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Verified!',
                  style: TextStyle(
                    fontFamily: 'roboto',
                    fontSize: 21,
                    fontWeight: FontWeight.w500,
                    color: Colors.black,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'you have succsefully verified the ',
                  style: TextStyle(
                    fontFamily: 'roboto',
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(78, 77, 77, 1),
                  ),
                ),Text(
                  'account.',
                  style: TextStyle(
                    fontFamily: 'roboto',
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(78, 77, 77, 1),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: 200,
                  height: 50,
                  child: ElevatedButton(
                    onPressed: () {
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(builder: (context) => forgotpass()),
                      // );
                    },
                    child: Text(
                      'Go To Dashboard',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      primary: Color.fromRGBO(238, 129, 116, 1),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}